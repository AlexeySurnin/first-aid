//@ts-check
import ObjectManager from './ObjectManager';

const Scene = require('Scene');
const Textures = require('Textures');
const TouchGestures = require('TouchGestures');
const Diagnostics = require('Diagnostics');

export default class Item {
    /**
     * Creates an instance of Item.
     * @param {Object} build
     * @param {String} build.name
     * @param {TextureBase} build.texture
     * @param {SceneObjectBase} build.sceneObject
     * @param {SceneObjectBase} build.pickupCollider
     * @memberof Item
     */
    constructor(build) {
        this.name = build.name;
        this.texture = build.texture;
        this.sceneObject = build.sceneObject;
        this.pickupCollider = build.pickupCollider;
    }

    static get Builder() {
        let build = {
            name: undefined,
            texture: undefined,
            sceneObject: undefined,
            pickupCollider: undefined
        };
        return (new class ItemBuilder {
            name(value) {
                build.name = value;
                return this;
            }
            texture(name) {
                build.texture = Textures.get(name);
                return this;
            }
            sceneObject(name) {
                build.sceneObject = ObjectManager.getObject(name);
                return this;
            }
            pickupCollider(name) {
                build.pickupCollider = ObjectManager.getObject(name);
                return this;
            }
            get ready() {
                for (let key in build) {
                    if (build[key] === undefined) {
                        Diagnostics.log('ERROR: Some of Item.Builder of item \'' + build.name + '\' methods was not used.');
                    }
                }
                return new Item(build);
            }
        });
    }
}
