const Patches = require('Patches');
const Reactive = require('Reactive');
const Diagnostics = require('Diagnostics');

export default class Animations {
    static get clips() {
        return class animationsDictionary {
            static get REANIMATION_BEGIN() { return 'CPR_HANDS_READY' }
            static get SHIRT_TEARING() { return 'SHIRT_TEARING' }
            static get CPR_PUSH() { return 'CPR_PUSH' }
            
            static get HEAD_RB_START() { return 'HEAD_RB_START' }
            static get HEAD_RB_TILT_BACK() { return 'HEAD_RB_TILT_BACK' }
            static get HEAD_RB_TILT_STRAIGHT() { return 'HEAD_RB_TILT_STRAIGHT' }
            static get HEAD_RB_STOP() { return 'HEAD_RB_STOP' }
        }
    }

    static play(animation) {
        switch (animation) {
            case 'CPR_HANDS_READY':
                Patches.setScalarValue('man_anim', 0);
                break;
            case 'SHIRT_TEARING':
                Patches.setScalarValue('man_anim', 1);
                break;
            case 'CPR_PUSH':
                Patches.setScalarValue('man_anim', 2);
                break;
            case 'HEAD_RB_START':
                Patches.setScalarValue('man_anim', 3);
                break;
            case 'HEAD_RB_TILT_BACK':
                Patches.setScalarValue('man_anim', 4);
                break;
            case 'HEAD_RB_TILT_STRAIGHT':
                Patches.setScalarValue('man_anim', 5);
                break;
            case 'HEAD_RB_STOP':
                Patches.setScalarValue('man_anim', 6);
                break;
        }

        Diagnostics.log('Case: ' + animation);

        Patches.setPulseValue('man_anim_updated', Reactive.once());
    }
}