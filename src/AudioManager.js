const Diagnostics = require('Diagnostics');
const Audio = require('Audio');
const Scene = require('Scene');
const Reactive = require('Reactive');

import Loader from './Loader';
import ObjectManager from 'areyes.sdk/src/ObjectManager';

let speakerFolder;
let volume = 1;
let audioObjects = [];

/**
 * Менеджер звуков
 *
 * @export
 * @class AudioManager
 */
export default class AudioManager {
    /**
     * Воспроизвести звук
     *
     * @static
     * @param {String} name Название контроллера звука
     * @memberof AudioManager
     */
    static play(name) {
        this._getAudioObject(name).play();
    }

    /**
     * Остановить проигрование звука
     *
     * @static
     * @param {String} name Название контроллера звука
     * @memberof AudioManager
     */
    static stop(name) {
        this._getAudioObject(name).stop();
    }

    /**
     * Зациклить проигрывание звука
     *
     * @static
     * @param {String} name Название контроллера звука
     * @memberof AudioManager
     */
    static loop(name) {
        this._getAudioObject(name).loop();
    }

    /**
     * Перезапустить проигрованние звука
     *
     * @static
     * @param {String} name Название контроллера звука
     * @memberof AudioManager
     */
    static reset(name) {
        this._getAudioObject(name).reset();
    }

    /**
     * Установить громкость
     *
     * @static
     * @param {number} value Значение от 0 до 1
     * @memberof AudioManager
     */
    static setVolume(value) {
        volume = value;
        audioObjects.forEach(object => object._updateVolume())
    }

    /**
     * Установить папку с Speakers
     *
     * @static
     * @param {String} folder Путь к папку
     * @memberof AudioManager
     */
    static setSpeakerFolder(folder) {
        speakerFolder = folder;
    }

    /**
     * Билдер менеджера
     *
     * @readonly
     * @static
     * @memberof AudioManager
     */
    static get Builder() {
        return new AudioManagerBuilder();
    }

    static _initialize() {
        if (speakerFolder === undefined) {
            Diagnostics.log('ERROR: Папка с Speaker\'ами не установлена. Используйте AudioManager.setSpeakerFolder()');
            return;
        }

        Loader.addDeferredAsyncCallback(() => {
            let promise = new Promise((resolve, reject) => {
                Scene.root.findByPath(ObjectManager.root + speakerFolder + '/*').then(speaker_result => {
                    if (speaker_result.length === 0) {
                        Diagnostics.log('ERROR: Папка по адресу \'' + speakerFolder + 
                            '\' не найдена или содержит 0 элементов');
                    }

                    if (speaker_result.length !== audioObjects.length) {
                        Diagnostics.log('ERROR: Количество Speaker (' + speaker_result.length + ') на сцене не совпадает '
                        + 'с количеством AudioObjects (' + audioObjects.length + ')');
                    }

                    speaker_result.forEach(speaker => {
                        let object = audioObjects.filter(a => a.speakerName === speaker.name)[0];
                        object.setSpeaker(speaker);
                    });
                });

                resolve();
            });

            return promise;
        });

        return Promise.resolve();
    }

    static _addAudioObject(audioObject) {
        audioObjects.push(audioObject);
    }

    /**
     * Получить контроллер по имени
     *
     * @static
     * @param {String} name Название контроллера звука
     * @returns {AudioObject}
     * @memberof AudioManager
     */
    static _getAudioObject(name) {
        let effect = audioObjects.filter(a => a.playbackControllerName === name)[0];
        if (effect === undefined) Diagnostics.log('ERROR: Аудиоконтроллер ' + name + ' не найден');
        return effect;
    }
}

class AudioManagerBuilder {
    /**
     * Добавить контроллер в кэш
     *
     * @param {String} playbackControllerName
     * @returns
     * @memberof AudioManagerBuilder
     */
    addAudioObject(playbackControllerName) {
        return new AudioObject.Builder(this, playbackControllerName);
    }

    /**
     * Установить общую громкость
     *
     * @param {Number} value Значение от 0 до 1
     * @returns
     * @memberof AudioManagerBuilder
     */
    setVolume(value) {
        volume = value;

        return this;
    }

    /**
     * Завершить настройку
     *
     * @readonly
     * @memberof AudioManagerBuilder
     */
    get ready() {
        Loader.addLoadableItem(AudioManager);
        return;
    }
}

/**
 * 
 *
 * @class AudioObject
 */
class AudioObject {
    constructor(build) {
        /** @type {String} */
        this.playbackControllerName = build._playbackControllerName;
        /** @type {PlaybackController} */
        this.playbackController = build._playbackController;
        /** @type {Number} */
        this.volume = build._volume;

        if (build.speakerName !== undefined) this.speakerName = build._speakerName;
        else this.speakerName = this.playbackControllerName;
    }

    /**
     * Воспроизвести
     *
     * @memberof AudioObject
     */
    play() {
        this.reset();
        // @ts-ignore
        this.playbackController.setPlaying(true);
    }

    /**
     * Остановить проигравание
     *
     * @memberof AudioObject
     */
    stop() {
        this.reset();
        this.playbackController.setLooping(false);
        this.playbackController.setPlaying(false);
    }

    /**
     * Перезапустить
     *
     * @memberof AudioObject
     */
    reset() {
        this.playbackController.reset();
    }

    /**
     * Зациклить
     *
     * @memberof AudioObject
     */
    loop() {
        this.reset();
        // @ts-ignore
        this.playbackController.setPlaying(true);
        // @ts-ignore
        this.playbackController.setLooping(true);
    }

    /**
     * Установить спикер
     *
     * @param {*} speaker
     * @memberof AudioObject
     */
    setSpeaker(speaker) {
        this.speaker = speaker;
        this._updateVolume();
    }

    /**
     * Установить громкость
     *
     * @param {number} value Значение от 0 до 1
     * @memberof AudioObject
     */
    setVolume(value) {
        this.volume = value;
        this._updateVolume();
    }

    _updateVolume() {
        this.speaker.volume = Reactive.val(this.volume * volume);
    }

    /**
     * Билдер
     *
     * @readonly
     * @static
     * @memberof AudioObject
     */
    static get Builder() {
        return AudioObjectBuilder;
    }
}

class AudioObjectBuilder {
    constructor(audioManagerBuilder, playbackControllerName) {
        this._audioManagerBuilder = audioManagerBuilder;

        this._playbackController = Audio.getPlaybackController(playbackControllerName);
        this._playbackControllerName = playbackControllerName;

        this._volume = 1;
    }

    /**
     * Установить имя спикера
     *
     * @param {String} speakerName Имя спикера на сцене
     * @returns
     * @memberof AudioObjectBuilder
     */
    setSpeaker(speakerName) {
        this._speakerName = speakerName;
        return this;
    }

    /**
     * Установить громкость
     *
     * @param {number} [value=1] Громкость. От 0 до 1
     * @returns
     * @memberof AudioObjectBuilder
     */
    setVolume(value = 1) {
        this._volume = value;
        return this;
    }

    /**
     * Зациклить звук при инициализации
     *
     * @returns
     * @memberof AudioObjectBuilder
     */
    setLoop() {
        this._playbackController.reset();
        // @ts-ignore
        this._playbackController.setPlaying(true);
        // @ts-ignore
        this._playbackController.setLooping(true);
        return this;
    }

    /**
     * Завершить настройку
     * 
     * @readonly
     * @returns {AudioManagerBuilder}
     * @memberof AudioObjectBuilder
     */
    get ready() {
        AudioManager._addAudioObject(new AudioObject(this));
        return this._audioManagerBuilder;
    }
}
