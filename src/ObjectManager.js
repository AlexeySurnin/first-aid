//@ts-check
const Scene = require('Scene');
const Diagnostics = require('Diagnostics');

let cachedSceneObjects = {};
let findOperations = [];

/**
 * Менеджер объектов на сцене. Позволяет кэшировать ссылки на объекты
 *
 * @export
 * @class ObjectManager
 * @extends {StaticLoadableItem}
 */
export default class ObjectManager {
    static _initialize() {
        return Promise.all(findOperations.map((op) => op()));
    }

    /**
     * Строитель менеджера объектов
     *
     * @readonly
     * @static
     * @memberof ObjectManager
     */
    static get Builder() {
        class ObjectManagerBuilder {
            /**
             * Кэшировать ссылку на объекты на сцене
             *
             * @param {String} shortcut Название ссылки
             * @param {String[]} pathes Пути к объектам (например: entities/entity1, entities/entity2)
             * @returns
             * @memberof ObjectManagerBuilder
             */
            load(shortcut, ...pathes) {
                pathes.forEach(path => {
                    let root = 'Device/';
                    findOperations.push(() => {
                        return Scene.root.findByPath(root + path).then((result) => {
                            if (result.length === 0) {
                                Diagnostics.log('ERROR: Объект \'' + shortcut + '\' по адресу \'' + path + '\' не найден');
                            }
                            if (cachedSceneObjects[shortcut] === undefined) cachedSceneObjects[shortcut] = [];
                            cachedSceneObjects[shortcut].push(result[0]);
                        });
                    });                    
                });

                return this;
            }
            /**
             * Завершает настройку менеджера объектов
             *
             * @readonly
             * @memberof ObjectManagerBuilder
             */
            get ready() {
                return ObjectManager._initialize();
            }
        }

        return new ObjectManagerBuilder();
    }

    /**
     * Возвращает кэшированный объект на сцене
     *
     * @static
     * @param {String} shortcut Название объекта
     * @returns {SceneObjectBase}
     * @memberof ObjectManager
     */
    static getObject(shortcut) {
        return this.getObjects(shortcut)[0];
    }

    /**
     * Возвращает массив кэшированных объектов на сцене
     *
     * @static
     * @param {String} shortcut Название группы объектов
     * @returns {SceneObjectBase[]}
     * @memberof ObjectManager
     */
    static getObjects(shortcut) {
        return cachedSceneObjects[shortcut];
    }
}
